
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Formaldehyde
   
 Example Purpose:  
----------------------------------
  - This example showcase the transformation of normal coordinates 
    into hybrid optimized and localized coordinates by means of the 
    Jacobi sweep algorithm with the use of a combined convergence criterium 
    of geometrical localization measure and VSCF energy.
   
 Additional Notes: 
----------------------------------

