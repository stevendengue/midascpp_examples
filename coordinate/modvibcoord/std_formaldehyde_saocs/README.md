
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Formaldehyde
   
 Example Purpose:  
----------------------------------
  - This example showcase the transformation of normal coordinates 
    into state-average optimized coordinates by means of the Jacobi sweep 
    algorithm with the use of a VSCF energy as convergence criterium.
   
 Additional Notes: 
----------------------------------

