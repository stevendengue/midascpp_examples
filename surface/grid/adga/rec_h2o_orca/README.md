
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Water
   
 Electronic structure:
----------------------------------
  - HF-3c  
   
 Electronic structure program:
----------------------------------
  - ORCA
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy and dipole surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 1-mode couplings. 
  
 Additional notes: 
----------------------------------

