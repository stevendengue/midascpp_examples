   
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Water
   
 Electronic structure:
----------------------------------
  - RI-MP2/cc-pVDZ, (energy and relaxed dipole moments)
   
 Electronic structure program:
----------------------------------
  - Turbomole
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy and dipole surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 2-mode couplings. 
  
 Additional notes: 
----------------------------------


