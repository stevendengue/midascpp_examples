
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - F2
   
 Electronic structure:
----------------------------------
  - CCSD(F12\*)(T)/cc-pVDZ-F12
   
 Electronic structure program:
----------------------------------
  - Turbomole
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 1-mode couplings. 
  
 Additional notes: 
----------------------------------

