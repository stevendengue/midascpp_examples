
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Water
   
 Electronic structure:
----------------------------------
  - CAM-B3LYP/6-31+g(d) 
   
 Electronic structure program:
----------------------------------
  - Gaussian
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 1-mode couplings. 
  
 Additional notes: 
----------------------------------

