
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - HSiF
   
 Electronic structure:
----------------------------------
  - CCSD/cc-pVDZ
   
 Electronic structure program:
----------------------------------
  - DALTON
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 1-mode couplings. 
  
 Additional notes: 
----------------------------------

