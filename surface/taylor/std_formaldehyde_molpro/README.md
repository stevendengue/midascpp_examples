
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Formaldehyde
   
 Electronic Structure:
----------------------------------
  - HF/cc-pVTZ-F12
   
 Electronic Structure Program:
----------------------------------
  - Molpro
   
 Example Purpose:  
----------------------------------
  - This example showcase the potential energy surface construction 
    by means of Taylor expansion to the second order.
   
 Additional Notes: 
----------------------------------
  - A Hessian matrix will be calculated for the potential energy surface 
    in order to generate normal coordinates.
