
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Water
   
 Electronic structure:
----------------------------------
  - CCSD(F12\*)(T)/cc-pVDZ-F12, (energy)
  - RI-MP2/cc-pVDZ-F12, (relaxed dipole moments)
   
 Electronic structure program:
----------------------------------
  - Turbomole
   
 Example purpose:  
----------------------------------
  - This example showcase the potential energy and dipole surface construction 
    by means of the adaptive density-guided approach (ADGA), which includes up to 2-mode couplings. 
  
 Additional notes: 
----------------------------------

