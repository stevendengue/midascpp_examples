#!/bin/bash
TESTNAME=std_formaldehyde_molpro

#######################################################################
#  Check that the electronic structure program is available
#######################################################################
MOLPROBIN=`echo $MOLPROBIN | cut -d '"' -f2`
if [ ! -x $MOLPROBIN/molpro ]; then
   echo $MOLPROBIN
   echo "molpro_bin path in ES_PROGRAMS MUST be right for this example to setup correctly"
   # 132 is run_example script error code!
   exit 132
fi

#######################################################################
#  Information file 
#######################################################################
cat > $TESTNAME\.info << %EOF%

   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Formaldehyde
   
 Electronic Structure:
----------------------------------
  - CCSD(T\*)-F12b/cc-pVTZ-F12
   
 Electronic Structure Program:
----------------------------------
  - Molpro
   
 Example Purpose:  
----------------------------------
  - This example showcase the potential energy surface construction 
    by means of Taylor expansion to the second order.
   
 Additional Notes: 
----------------------------------
  - A Hessian matrix will be calculated for the potential energy surface 
    in order to generate normal coordinates.

%EOF%

#######################################################################
#  Seup directories
#######################################################################

MAINDIR=$(pwd)

SAVEDIR=$MAINDIR/savedir
ANALYSISDIR=$MAINDIR/analysis
INTERFACEDIR=$MAINDIR/InterfaceFiles

mkdir $INTERFACEDIR

#######################################################################
#  Define interface file names
#######################################################################

ESPINPUT=InputCreatorScript.sh
ESPRUN=RunScript.sh
PROPINFO=midasifc.propinfo

#######################################################################
#  MidasCpp Input
#######################################################################
cat > $TESTNAME\.minp << %EOF%
#0 MidasInput

#1 General
#2 MainDir
$MAINDIR

#2 AnalysisDir
$ANALYSISDIR

#1 SinglePoint

#2 Name
generic_molpro_spc
#2 Type
SP_GENERIC
#2 InputCreatorScript
$ESPINPUT
#2 RunScript
$ESPRUN

#1 System

#2 SysDef
#3 MoleculeFile
Midas
$MAINDIR/$TESTNAME.mmol

#1 Pes

#2 CartesianCoord
#2 NoScalCoordInFit
#2 TaylorInfo
0 2 2
#2 Displacement
SimpleDisplacement
1.0e-3
#2 MultilevelPes
generic_molpro_spc 2
#2 GenHessian
1
Ground_state_energy
#2 NThreads
4

#0 MidasInputEnd
%EOF%

#######################################################################
#  Molecule input
#######################################################################
cat > $TESTNAME\.mmol << %EOF%
#0 MoleculeInput

#1 XYZ
4 au

O  0.000000000000E+00  0.000000000000E+00  2.256180684864E+00
C  0.000000000000E+00  0.000000000000E+00 -2.293037285400E-02
H  1.772179347124E+00  0.000000000000E+00 -1.116624171246E+00
H -1.772179347124E+00  0.000000000000E+00 -1.116624171246E+00

#0 MoleculeInputEnd
%EOF%

#######################################################################
#  Molpro input creator script
#######################################################################
cat > $INTERFACEDIR/$ESPINPUT << +EOF+
#!/bin/bash
#######################################################################
#                                                                     #
#  Interface script between MidasCpp and Molpro                       #
#                                                                     #
#  Electronic structure model: CCSD(T*)-F12b/cc-pVTZ-F12              #
#                                                                     #
#  Purpose: Create a Molpro input file with an xyz coordinate file    #
#                                                                     #
#######################################################################

# Define the input script
setup_molpro() {
cat << %EOF% > singlepoint.minp
***, MidasCpp generated input for Molpro.
geomtyp=xyz
geometry
%EOF%

# Obtain the number of coordinates
n_atoms=\`cat midasifc.xyz_input | wc -l\`
n_atoms=\`expr \$n_atoms - 2\`
echo \$n_atoms >> singlepoint.minp

# Add a comment line
cat << %EOF% >> singlepoint.minp
Below follows the Molpro input
%EOF%

# The first lines contain the structure in standard xyz format
tail -n\$n_atoms midasifc.xyz_input \\
    | sed -e 's/^ //g' \\
    | sed -e 's/[0-9][0-9][0-9][0-9][0-9][0-9]e/e/g' \\
    | sed -e 's/[0-9][0-9][0-9][0-9][0-9][0-9]E/E/g' \\
    | sed 's/e/d/g' >> singlepoint.minp

# Thereafter follows the electronic structure calculation specification
cat << %EOF% >> singlepoint.minp
end

gthresh,energy=1.d-12,orbital=1.d-12,gradient=1.d-12
maxit=200
basis=cc-pVTZ-F12
{hf,energy=1.d-12,accu=1.d-14,maxit=200}
{ccsd(t)-f12,scale_trip=1,maxit=200; core}

put,xyz,midasifc.cartrot_xyz,new
%EOF%
}

# Create the input script
setup_molpro
+EOF+

#######################################################################
#  Molpro input run script
#######################################################################
cat > $INTERFACEDIR/$ESPRUN << %EOF%
#!/bin/bash
#######################################################################
#                                                                     #
#  Interface script between MidasCpp and Molpro                       #
#                                                                     #
#  Electronic structure model: CCSD(T*)-F12b/cc-pVTZ-F12              #
#                                                                     #
#  Purpose: Run a Molpro input file and extract the final results     #
#                                                                     #
#######################################################################

# Define which properties should be extrated from electronic structure calculation
create_property_file() 
{
   # Extract the energy
   energy=\`grep "\!CCSD(T)-F12b total energy" singlepoint.mout | awk '{print \$4}'\`

   # Write property/properties to file for MidasCpp to read
   echo "GROUND_STATE_ENERGY     " \$energy > midasifc.prop_general
}

# Change directory to the scratch directory
cd \$1

# Now start the electronic structure calculation
$MOLPROBIN/molpro -d \$1 -m 120000000 -G 120000000 -o \$1/singlepoint.mout \$1/singlepoint.minp

# And extract property/properties
create_property_file
%EOF%

#######################################################################
#  MidasCpp property infomation file
#######################################################################
cat > $INTERFACEDIR/$PROPINFO << %EOF%
tens_order=(0),descriptor=(GROUND_STATE_ENERGY)
%EOF%

