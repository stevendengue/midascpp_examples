
   Example Calculation Overview
==================================

 Molecule:
----------------------------------
  - Formaldehyde

 Electronic Structure:
----------------------------------
  - CCSD/cc-pVTZ
   
 Electronic Structure Program:
----------------------------------
  - Molpro

 Example Purpose:  
----------------------------------
  - This example showcase the potential energy and dipole surface construction 
    by means of Taylor expansion to the fourth order. 
   
 Additional Notes: 
----------------------------------
  - The effective inertia inverse tensor is calculated
  - Vibrational polarizability is calculated

