
X-TDH wave packet propagation on H2O surface
==============

Info on X-TDH
--------------

- Properties: Energy, phase, auto-correlation functions
- Expectation values: q, p
- Spectra: Auto-correlation spectrum

