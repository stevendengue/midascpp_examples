
L-TDH wave packet propagation on H2O surface using spectral basis of q operator
==============

Info on L-TDH
--------------

- Properties: Energy, phase, auto-correlation functions
- Expectation values: q, p
- Spectra: Auto-correlation spectrum

