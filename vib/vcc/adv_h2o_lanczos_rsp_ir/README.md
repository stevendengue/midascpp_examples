#######################################################################
#                                                                     #
#  Example Calculation Overview                                       #
#                                                                     #
#######################################################################

   Molecule:
      Water
   
   Functionality:
      VCC[2]
   
   Example purpose:  
      This example showcases how to do Lanczos damped response.
      For the sake of speed this example uses a VCC[2] ground-state, 
      but is not limited to this choice.
 
   Additional notes: 
      For original publication see [https://doi.org/10.1039/C3CP50283J].

