#######################################################################
#                                                                     #
#  Example Calculation Overview                                       #
#                                                                     #
#######################################################################

   Molecule:
      Water
   
   Functionality:
      VCC[2]
   
   Example purpose:  
      This example showcases how to do damped response in a frequency interval.
      For the sake of speed this example uses a VCC[2] ground-state, 
      but is not limited to this choice.
 
   Additional notes: 
      For original publication see [https://doi.org/10.1063/1.4932010].
      
      Running time is roughly 1 min 30 sec.
      
      MidasCpp will produce an analysis of the IR spectrum.
      This is presented in the mout file (look for "Midas Analysis Program").
      In addition to that MidasCpp will also generate response data output files:
         analysis/rsp_ir_spec.dat
         analysis/rsp_ir_stick_min.dat
         analysis/rsp_real.dat
         analysis/rsp_imag.dat  
         analysis/rsp_ir_stick.dat  
         analysis/rsp_isotropic.dat

