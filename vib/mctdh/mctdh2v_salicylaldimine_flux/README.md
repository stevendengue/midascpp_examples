
MR-MCTDH[2,V] wave packet propagation on 6D salicylaldimine surface
==============

Info on MCTDH
--------------

- Properties: Energy, auto-correlation function
- Expectation values: flux over transition state

