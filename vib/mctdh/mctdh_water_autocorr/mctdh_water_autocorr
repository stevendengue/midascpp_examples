#!/bin/bash

NAME="${0}"

#Function make_dir, which creates directory.
make_dir() {
mkdir $1 >& /dev/null #Creates dir, sends output and error to trash to avoid obsolete output messages.
}

# make analysis directory
make_dir analysis

echo "Preparing files for example: "$NAME

cat > ${NAME}.info <<%EOF%
#######################################################################
#                                                                     #
#  Example Calculation Overview                                       #
#                                                                     #
#######################################################################

   Molecule:
      Water
   
   Functionality:
      MCTDH wave packet propagation on water calculating autocorrelation function
   
   Example purpose:  
      This example showcases how to calculate an auto-correlation function using MCTDH.
 
   Additional notes: 

%EOF%



cat > ${NAME}.minp << %EOF%
#0 MIDAS Input 

// General input 
#1 general

// Set general (minimum) IO level
#2 IoLevel
    5 

// Vibrational input
#1 Vib

// Hamiltonian operator for generating the initial wave packet
#2 Operator
   #3 Name
      H_REF
   // Operator file (in this case contains only the PES, but kinetic energy is added by default)
   #3 OperFile
      ${NAME}_ref.mop
   // Set type (i.e. add kinetic energy)
   #3 SetInfo
      type=energy

// Hamiltonian operator for MCTDH propagation
#2 Operator
   #3 Name
      H_MCTDH
   // Operator file (in this case contains only the PES, but kinetic energy is added by default)
   #3 OperFile
      ${NAME}.mop
   // Set type (i.e. add kinetic energy)
   #3 SetInfo
      type=energy

// Basis input
#2 Basis
   #3 Name
      hobas
   // Use HO basis functions (up to n=40)
   #3 HoBasis
      40

// VSCF input for generating the initial wave packet.
#2 Vscf
   #3 Oper
      H_REF

// MCTDH input
#2 McTdH
   // Set a name for the calculation
   #3 Name
      mctdh
   // Use full MCTDH method
   #3 Method
      MCTDH
   // Use 4 time-dependent modals per mode
   #3 ActiveSpace
      [3*4]
   // Set basis (must be the same as in VSCF)
   #3 Basis
      hobas
   // Set operator for propagation
   #3 Oper
      H_MCTDH
   // Define ODE integrator
   #3 Integrator
      // Use variable mean field
      #4 Scheme
         VMF
      // Read info for integrator
      #4 OdeInfo
         // Use the MidasCpp implementation of Dormand-Prince 8(5,3) explicit Runge-Kutta method
         #5 Type
            MIDAS DOPR853
         // Integrate from t=0 to t=1000 a.u.
         #5 TimeInterval
            0. 2.5e3
         // Save 2501 equidistant time points (including the one at t=0)
         #5 OutputPoints
            2501
         // Set accuracy (abs, rel)
         #5 Tolerance
            1.e-10 1.e-10
         // Set initial step size
         #5 InitialStepSize
            1.e-4
   // Calculate properties
   #3 Properties
      energy
      halftimeautocorr
      densanalysis
   // Calculate spectrum of half-time auto-correlation function
   #3 Spectra
      halftimeautocorr

#0 Midas Input End
%EOF%

# H operator for generating initial wave packet
cat > ${NAME}_ref.mop << %EOF%
#0MIDASMOPINPUT
#1OPERATORTERMS
 1.5756874643102492000000E-04   Q^2(Q0)   
 1.0000000000000000000000E-03   Q^1(Q1)   
 1.4937195226139011000000E-04   Q^2(Q1)   
 2.7465351735145305000000E-05   Q^2(Q2)   
#1MODENAMES
Q0   Q1   Q2   
#0MIDASMOPINPUTEND
%EOF%

# H operator for MCTDH propagation
cat > ${NAME}.mop << %EOF%
#0MIDASMOPINPUT
#1OPERATORTERMS
-1.3983481039758772000000E-11   Q^1(Q0)   
 1.5756874643102492000000E-04   Q^2(Q0)   
-1.3022827261011116000000E-10   Q^3(Q0)   
 4.6707384626643034000000E-08   Q^4(Q0)   
 4.5483926669476205000000E-07   Q^1(Q1)   
 1.4937195226139011000000E-04   Q^2(Q1)   
 3.0827675345790340000000E-06   Q^3(Q1)   
 4.3788077164208516000000E-08   Q^4(Q1)   
 3.7033530020380567000000E-08   Q^1(Q2)   
 2.7465351735145305000000E-05   Q^2(Q2)   
 1.4056156771857786000000E-07   Q^3(Q2)   
-5.4826898576720851000000E-10   Q^4(Q2)   
 2.2424728740588762000000E-10   Q^1(Q0)   Q^1(Q1)   
 1.3244516594568267000000E-10   Q^1(Q0)   Q^2(Q1)   
 1.0004441719502211000000E-11   Q^1(Q0)   Q^3(Q1)   
 9.5331590728164883000000E-06   Q^2(Q0)   Q^1(Q1)   
 2.7097757993033156000000E-07   Q^2(Q0)   Q^2(Q1)   
-4.0927261579781771000000E-12   Q^3(Q0)   Q^1(Q1)   
 1.6018475434975699000000E-10   Q^1(Q0)   Q^1(Q2)   
 1.0061285138363019000000E-11   Q^1(Q0)   Q^2(Q2)   
-1.2647660696529783000000E-12   Q^1(Q0)   Q^3(Q2)   
-9.1266440449544461000000E-07   Q^2(Q0)   Q^1(Q2)   
-5.4972815632936545000000E-08   Q^2(Q0)   Q^2(Q2)   
-7.3896444519050419000000E-13   Q^3(Q0)   Q^1(Q2)   
-2.8100544113840442000000E-10   Q^1(Q2)   Q^1(Q1)   
-7.2744757062537246000000E-07   Q^2(Q2)   Q^1(Q1)   
 9.8503392109705601000000E-09   Q^3(Q2)   Q^1(Q1)   
-2.2016263301338768000000E-07   Q^1(Q2)   Q^2(Q1)   
-4.5356273403740488000000E-08   Q^2(Q2)   Q^2(Q1)   
-9.0807930064329412000000E-09   Q^1(Q2)   Q^3(Q1)   
-4.7748471843078732000000E-12   Q^1(Q0)   Q^1(Q2)   Q^1(Q1)   
 3.7516656448133290000000E-12   Q^1(Q0)   Q^2(Q2)   Q^1(Q1)   
-9.3223206931725144000000E-12   Q^1(Q0)   Q^1(Q2)   Q^2(Q1)   
-5.4205202104640193000000E-08   Q^2(Q0)   Q^1(Q2)   Q^1(Q1)   
#1MODENAMES
Q0   Q1   Q2   
#0MIDASMOPINPUTEND
%EOF%
