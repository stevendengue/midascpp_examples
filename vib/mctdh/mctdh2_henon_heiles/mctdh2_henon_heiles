#!/bin/bash

NAME="${0}"

#Function make_dir, which creates directory.
make_dir() {
mkdir $1 >& /dev/null #Creates dir, sends output and error to trash to avoid obsolete output messages.
}

# make analysis directory
make_dir analysis

echo "Preparing files for example: "$NAME

cat > ${NAME}.info <<%EOF%
#######################################################################
#                                                                     #
#  Example Calculation Overview                                       #
#                                                                     #
#######################################################################

   Molecule:
      6D Henon-Heiles
   
   Functionality:
      MCTDH[2,D] and MCTDH[2,V] wave-packet propagation.
   
   Example purpose:  
      This example showcases how to propagate wave packets using the MCTDH[n] methods.
 
   Additional notes: 

%EOF%



cat > ${NAME}.minp << %EOF%
#0 MIDAS Input 

// General input 
#1 general

// Set general (minimum) IO level
#2 IoLevel
    5 

// Vibrational input
#1 Vib

// Hamiltonian operator for generating the initial wave packet
#2 Operator
   #3 Name
      H_REF
   // Operator file (in this case contains only the PES, but kinetic energy is added by default)
   #3 OperFile
      ${NAME}_ref.mop
   // Set type (i.e. add kinetic energy)
   #3 SetInfo
      type=energy

// Hamiltonian operator for MCTDH propagation
#2 Operator
   #3 Name
      H_MCTDH
   // Operator file (in this case contains only the PES, but kinetic energy is added by default)
   #3 OperFile
      ${NAME}.mop
   // Set type (i.e. add kinetic energy)
   #3 SetInfo
      type=energy

// Q operator
#2 Operator
   #3 Name
      q
   // Define operator directly in input file
   #3 Operinput
      #4 Operatorterms
         1.0 Q^1(Q0)
         1.0 Q^1(Q1)
         1.0 Q^1(Q2)
         1.0 Q^1(Q3)
         1.0 Q^1(Q4)
         1.0 Q^1(Q5)

// i*p operator (since complex operators are not supported)
#2 Operator
   #3 Name
      ip
   // Define operator directly in input file
   #3 Operinput
      #4 Operatorterms
         1.0 DDQ^1(Q0)
         1.0 DDQ^1(Q1)
         1.0 DDQ^1(Q2)
         1.0 DDQ^1(Q3)
         1.0 DDQ^1(Q4)
         1.0 DDQ^1(Q5)

// Basis input
#2 Basis
   #3 Name
      bsplbas
   // Use 10th-order B-splines
   #3 BsplineBasis
      10
   // Set basis boundaries to classical turning point of n=20 HO function for each mode
   #3 TurningPoint
      20
   // Use 50 basis functions per mode
   #3 NPrimBasisFunctions
      50

// VSCF input for generating the initial wave packet.
#2 Vscf
   #3 Name
      vscf
   #3 Basis
      bsplbas
   #3 Oper
      H_REF

// MCTDH[2,D] input
#2 McTdH
   // Set a name for the calculation
   #3 Name
      mctdh2d
   // Use MCTDH[2,D] method
   #3 Method
      MCTDH[2,D]
   // Use 4 time-dependent modals per mode
   #3 ActiveSpace
      [6*4]
   // Set basis (must be the same as in VSCF)
   #3 Basis
      bsplbas
   // Set operator for propagation
   #3 Oper
      H_MCTDH
   // Define ODE integrator
   #3 Integrator
      // Use variable mean field
      #4 Scheme
         VMF
      // Read info for integrator
      #4 OdeInfo
         // Use the MidasCpp implementation of Dormand-Prince 8(5,3) explicit Runge-Kutta method
         #5 Type
            MIDAS DOPR853
         // Integrate from t=0 to t=10 a.u.
         #5 TimeInterval
            0. 1.e1
         // Save 101 equidistant time points (including the one at t=0)
         #5 OutputPoints
            101
         // Set accuracy (abs, rel)
         #5 Tolerance
            1.e-10 1.e-10
         // Set initial step size
         #5 InitialStepSize
            1.e-4
   // Calculate properties
   #3 Properties
      energy
      halftimeautocorr
   // Calculate expectation values of q and i*p (take the imaginary part to get <p>)
   #3 ExpectationValues
      q ip

// MCTDH[2,V] input
#2 McTdH
   // Set a name for the calculation
   #3 Name
      mctdh2v
   // Use MCTDH[2,V] method
   #3 Method
      MCTDH[2,V]
   // Use 4 time-dependent modals per mode
   #3 ActiveSpace
      [6*4]
   // Set basis (must be the same as in VSCF)
   #3 Basis
      bsplbas
   // Set operator for propagation
   #3 Oper
      H_MCTDH
   // Define ODE integrator
   #3 Integrator
      // Use variable mean field
      #4 Scheme
         VMF
      // Read info for integrator
      #4 OdeInfo
         // Use the MidasCpp implementation of Dormand-Prince 8(5,3) explicit Runge-Kutta method
         #5 Type
            MIDAS DOPR853
         // Integrate from t=0 to t=10 a.u.
         #5 TimeInterval
            0. 1.e1
         // Save 101 equidistant time points (including the one at t=0)
         #5 OutputPoints
            101
         // Set accuracy (abs, rel)
         #5 Tolerance
            1.e-10 1.e-10
         // Set initial step size
         #5 InitialStepSize
            1.e-4
   // Calculate properties
   #3 Properties
      energy
      halftimeautocorr
   // Calculate expectation values of q and i*p (take the imaginary part to get <p>)
   #3 ExpectationValues
      q ip

#0 Midas Input End
%EOF%

# H operator for generating initial wave packet
cat > ${NAME}_ref.mop << %EOF%
#0MIDASMOPINPUT
#1CONSTANTS
#1FUNCTIONS
#1OPERATORTERMS
 0.5 Q^2(Q0)
-0.5 Q^1(Q0)
 0.5 Q^2(Q1)
-0.5 Q^1(Q1)
 0.5 Q^2(Q2)
-0.5 Q^1(Q2)
 0.5 Q^2(Q3)
-0.5 Q^1(Q3)
 0.5 Q^2(Q4)
-0.5 Q^1(Q4)
 0.5 Q^2(Q5)
-0.5 Q^1(Q5)
#1MODENAMES
Q0 Q1 Q2 Q3 Q4 Q5 
#0MIDASMOPINPUTEND
%EOF%

# H operator for MCTDH propagation
cat > ${NAME}.mop << %EOF%
#0MIDASMOPINPUT
#1CONSTANTS
#1FUNCTIONS
#1OPERATORTERMS
 0.5 Q^2(Q0)
 0.5 Q^2(Q1)
 0.5 Q^2(Q2)
 0.5 Q^2(Q3)
 0.5 Q^2(Q4)
 0.5 Q^2(Q5)
 0.111803 Q^2(Q0) Q^1(Q1)
-0.0372676666667 Q^3(Q1)
 0.111803 Q^2(Q1) Q^1(Q2)
-0.0372676666667 Q^3(Q2)
 0.111803 Q^2(Q2) Q^1(Q3)
-0.0372676666667 Q^3(Q3)
 0.111803 Q^2(Q3) Q^1(Q4)
-0.0372676666667 Q^3(Q4)
 0.111803 Q^2(Q4) Q^1(Q5)
-0.0372676666667 Q^3(Q5)
#1MODENAMES
Q0 Q1 Q2 Q3 Q4 Q5 
#0MIDASMOPINPUTEND
%EOF%
